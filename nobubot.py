#!/bin/env python3
import discord
import configparser
import shlex
import re

config = configparser.ConfigParser()
config.read('config.ini')

TOKEN = config['auth']['token']

client = discord.Client()

COLORSTRIPE=0xeee657

@client.event
async def on_ready():
    print('Logged on as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user or message.author.bot == True:
        return

    if message.content.startswith(client.user.mention):
        await parse_command(message)

PRONOUNS_PREFIX="pronouns: "
COLOR_PREFIX="color: "
async def set_custom_role(author, name, prefix, color=None):
    if len(prefix) == 0:
        raise Exception("prefix needs to be longer than 0 characters.")

    guild = author.guild

    role_name = "{1}{0}".format(name, prefix)

    role = None
    for r in guild.roles:
        if r.name == role_name:
            role = r
            break

    if color == None:
        color = discord.Colour.default()

    if name != "":
        if role == None:
            role = await guild.create_role(name=role_name, colour=color, reason="added pronoun role: {0}".format(name))
        await author.add_roles(role, reason="requested by user.")

    delete_roles = []
    for r in author.roles:
        if r != role:
            if r.name.startswith(prefix):
                delete_roles.append(r)
    await author.remove_roles(*delete_roles,  reason="removing old pronouns.")
    # TODO: Check if the roles are now empty of members and prune them.

    return True


async def parse_command(message):
    author = message.author

    try:
        args = shlex.split(message.content[len(client.user.mention):])

        if len(args) == 0:
            args.append("help")

        command = args[0].lower()

        if command == "help":
            embed = discord.Embed(title="cute mechanical vampire nobubot!", description="preface the commands with `@{0} `, like this: `@{0} help`.\narguments to commands are interpreted like bash, because nobubot is a big nerd. this means that you'll have to quote arguments with spaces in them.".format(client.user.name), color=COLORSTRIPE)
            embed.add_field(name="info", value="info about this bot", inline=False)
            embed.add_field(name="help", value="this list of commands", inline=False)
            embed.add_field(name="pronouns <pronouns> [pronouns]...", value="set your pronouns that are visible on your profile", inline=False)
            embed.add_field(name="color [#]<hex>", value="set a custom color value for your name", inline=False)
            embed.set_thumbnail(url="https://i.imgur.com/L2a1uu1.jpg")
            await message.channel.send(embed=embed)

        elif command == "info":
            embed = discord.Embed(title="cute mechanical vampire nobubot!", description=
"""an extremely cute vampire robot that helps manage this precious server! she likes donuts. give her donuts.

type `@{0} help` to see all commands.

made by <@106479519417802752> using [discord.py (v1 rewrite)](https://github.com/Rapptz/discord.py/tree/rewrite)
""".format(client.user.name), color=COLORSTRIPE)
            embed.set_thumbnail(url="https://i.imgur.com/L2a1uu1.jpg")
            await message.channel.send(embed=embed)

        elif re.match(r"^pronouns?$", command):
            pronouns = iter(args[1:])
            name = ', '.join(pronouns)
            if await set_custom_role(author, name, PRONOUNS_PREFIX):
                if name != "":
                    await message.channel.send(embed=discord.Embed(description="setting {0}'s pronouns to `{1}`".format(author.mention, name), color=COLORSTRIPE))
                else:
                    await message.channel.send(embed=discord.Embed(description="removing {0}'s pronouns".format(author.mention), color=COLORSTRIPE))

        elif re.match("^colou?rs?$", command):
            if len(args) <= 1:
                if await set_custom_role(author, "", COLOR_PREFIX):
                    await message.channel.send(embed=discord.Embed(description="removing {0}'s color.".format(message.author.mention), color=COLORSTRIPE))
                return

            color_match = re.match('^#?((?:[0-9a-fA-F]){3}|(?:[0-9a-fA-F]){6})$', args[1])
            if color_match == None or len(color_match.groups()) != 1:
                raise Exception("color needs to be hex, like `#ff00ff`.")

            color_hex = color_match.groups()[0].lower()
            if len(color_hex) == 3:
                color_hex = color_hex[0] * 2 + color_hex[1] * 2 + color_hex[2] * 2
            color_name = "#{0}".format(color_hex)
            color = discord.Colour.from_rgb(*bytes.fromhex(color_hex))
            if await set_custom_role(author, color_name, COLOR_PREFIX, color=color):
                await message.channel.send(embed=discord.Embed(description="setting {0}'s color to `{1}`.".format(author.mention, color_name), color=color))

        elif command == "send" and args[1] == "image" and author.permissions_in(message.channel).manage_messages:
            await message.delete()
            embed = discord.Embed(color=COLORSTRIPE)
            embed.set_image(url=args[2])
            await message.channel.send(embed=embed)

        elif command == "send" and args[1] == "message" and author.permissions_in(message.channel).manage_messages:
            await message.delete()
            embed = discord.Embed(description="{0}".format(' '.join(args[2:])), color=COLORSTRIPE)
            await message.channel.send(embed=embed)

        elif command.startswith("🍩"):
            embed = discord.Embed(title='🍩 DONUTS!! 🍩', description='nom nom nom', color=COLORSTRIPE)
            #TODO: Add multiple images and randomly select one
            embed.set_image(url="https://i.imgur.com/zVzY7hp.gif")
            await message.channel.send(embed=embed)

        elif command == "good" and args[1].startswith("bot"):
            embed = discord.Embed(color=COLORSTRIPE)
            embed.set_image(url="https://i.imgur.com/jyR8UFs.gif")
            await message.channel.send(embed=embed)

        elif command == "good" and args[1].startswith("night"):
            embed = discord.Embed(title="rest well my master", color=COLORSTRIPE)
            embed.set_image(url="https://i.imgur.com/JnUMWfe.gif")
            await message.channel.send(embed=embed)

        else:
            raise Exception("command not found:\n{0}".format(command))

    except Exception as err:
        embed = discord.Embed(description="`{0}`".format(str(err).lower()), color=0xcc0000)
        await message.channel.send(embed=embed)
        raise err

client.run(TOKEN)

